/*
 *  /---------------\
 *  | DATA RELOCATE |
 *  \---------------/
 */
var elements = document.getElementsByTagName('script');
var relocateElements = null;
for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    console.log(i + " -> " + element);
    if (element.dataset.relocate == "true") {
        element.removeAttribute('data-relocate');
        relocateElements += element;
        console.log('     \\-> TRUE');
        element.remove();
        /*var tElem = element;
         tElem.removeAttribute('data-relocate');
         document.body.innerHTML += tElem.outerHTML;*/
    } else {
        console.log('     \\-> FALSE');
    }
}
//document.body.innerHTML += relocateElements.outerHTML;