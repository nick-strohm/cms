<div class="col-md-3" id="module.sidebar sidebar">
    <div class="list-group">
        @foreach($sidebar['links'] as $item)
            <a href="{{ $item['url'] }}" class="list-group-item
@if(url($item['url']) == url()->current()) active
@endif ">{{ $item['name'] }}</a>
        @endforeach
    </div>
</div>