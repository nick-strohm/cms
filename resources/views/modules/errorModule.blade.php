<div class="container">
    @foreach($errors as $error)
        @if (isset($error['type']) && isset($error['text']))
            <div class="alert alert-{{$error['type']}}">{{$error['text']}}</div>
        @endif
    @endforeach
</div>