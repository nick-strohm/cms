{{-- module: modules.navbarItemModule --}}
@if(isset($type) && isset($url) && isset($name))
    @if($type == "single")
        <li class="@if(url($url) == url()->current())
                active
                @endif"><a href="{{url($url)}}">{{$name}}</a></li>
    @elseif ($type == "stacked")
        <li class="dropdown">
            <a href="{{url($url)}}" data-toggle="dropdown" role="button" aria-expanded="false">
                {{$name}} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                @foreach($items as $item)
                    @if ($item['type'] == "single")
                        @include('modules.navbarItemModule', $item)
                    @endif
                @endforeach
            </ul>
        </li>
    @endif
@endif