@push('scripts')
<script src="https://code.jquery.com/jquery-2.2.4.min.js" data-relocate="true"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" data-relocate="true"></script>
@endpush

{{-- layout: layouts.baseLayout --}}
<!doctype HTML>
<html lang="en">
    <head>
        <title>@hasSection('title')<?php echo env('APP_NAME', 'CMS') . " - "; ?>@yield('title')@else<?php echo env('APP_NAME', 'CMS'); ?>@endif</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://bootswatch.com/slate/bootstrap.min.css">
        @stack('scripts')
    </head>
    <body id="layout.base">
        @if (count($navbar) > 0)
            @include('modules.navbarModule', ['items' => $navbar])
        @endif


        @if (count($errors) > 0)
            @include('modules.errorModule', ['errors' => $errors])
        @endif

        @yield('layout')

        <script src="js/cms.js"></script>
    </body>
</html>