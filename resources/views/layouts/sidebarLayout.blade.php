{{-- layout: layouts.sidebarLayout --}}

@extends('layouts.baseLayout')

@section('layout')
    <div class="container" id="layout.sidebar">
        <div class="row row-offcanvas row-offcanvas-right">
            @if ($sidebar['orientation'] == "left")
                @include('modules.sidebarModule', $sidebar)
                <div class="col-md-9">
                    @yield('content')
                </div>
            @elseif($sidebar['orientation'] == "right")
                <div class="col-md-9">
                    @yield('content')
                </div>
                @include('modules.sidebarModule', $sidebar)
            @elseif($sidebar['orientation'] == 'none')
                <div class="col-md-12">
                    @yield('content')
                </div>
            @else
                <div class="col-md-9">
                    @yield('content')
                </div>
                @include('modules.sidebarModule', $sidebar)
            @endif
        </div>
    </div>
@endsection