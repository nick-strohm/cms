<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class setupInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the framework';

    protected $vars = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setupVars();

        $this->getAppData();
        $this->getDatabase();

        $variables = array();
        foreach($this->vars as $key => $value) {
            array_push($variables, array('key' => $key, 'value' => $value));
        }

        $this->table(['Key','Value'], $variables);

        if($this->confirm('Are your credentials correct?', true)) {
            $content = "";
            foreach($this->vars as $key => $value) {
                $content = $content.$key.'='.$value.PHP_EOL;
            }

            file_put_contents('.env', $content);

            $this->info('.env file updated!');
        } else {
            $this->error('Aborting');
        }

        $this->createDatabase();

        $this->info('Done...');
    }

    private function setupVars() {
        $this->vars['APP_ENV'] = env('APP_ENV');
        $this->vars['APP_KEY'] = env('APP_KEY');
        $this->vars['APP_DEBUG'] = env('APP_DEBUG');
        $this->vars['APP_LOG_LEVEL'] = env('APP_LOG_LEVEL');
        $this->vars['APP_URL'] = env('APP_URL');
        $this->vars['APP_NAME'] = env('APP_NAME');
        $this->vars['DB_CONNECTION'] = env('DB_CONNECTION');
        $this->vars['DB_HOST'] = env('DB_HOST');
        $this->vars['DB_PORT'] = env('DB_PORT');
        $this->vars['DB_DATABASE'] = env('DB_DATABASE');
        $this->vars['DB_USERNAME'] = env('DB_USERNAME');
        $this->vars['DB_PASSWORD'] = env('DB_PASSWORD');
        $this->vars['CACHE_DRIVER'] = env('CACHE_DRIVER');
        $this->vars['SESSION_DRIVER'] = env('SESSION_DRIVER');
        $this->vars['QUEUE_DRIVER'] = env('QUEUE_DRIVER');
        $this->vars['REDIS_HOST'] = env('REDIS_HOST');
        $this->vars['REDIS_PASSWORD'] = env('REDIS_PASSWORD');
        $this->vars['REDIS_PORT'] = env('REDIS_PORT');
        $this->vars['MAIL_DRIVER'] = env('MAIL_DRIVER');
        $this->vars['MAIL_HOST'] = env('MAIL_HOST');
        $this->vars['MAIL_PORT'] = env('MAIL_PORT');
        $this->vars['MAIL_USERNAME'] = env('MAIL_USERNAME');
        $this->vars['MAIL_PASSWORD'] = env('MAIL_PASSWORD');
        $this->vars['MAIL_ENCRYPTION'] = env('MAIL_ENCRYPTION');
    }

    private function getAppData() {

        $name = $this->ask('Enter your app name');
        $this->info('Your answer: '.$name);

        $url = $this->ask('Enter your app url', 'http://localhost');
        $this->info('Your answer: '.$url);

        if($this->confirm('Name: '.$name.PHP_EOL.' Url: '.$url.PHP_EOL.PHP_EOL.' Are your credentials correct?', true)) {
            $this->vars['APP_NAME'] = $name;
            $this->vars['APP_URL'] = $url;
        } else {
            $this->info(PHP_EOL.' Retrying');
            $this->getAppData();
        }
    }

    private function getDatabase()
    {
        $conn = $this->choice('Select your database driver', ['sqlite', 'mysql', 'pgsql'], 1);
        $this->info('Your answer: '.$conn);

        $host = $this->ask('Enter your database host', 'localhost');
        $this->info('Your answer: '.$host);

        $port = $this->ask('Enter your database port', 3306);
        $this->info('Your answer: '.$port);

        $data = $this->ask('Enter your database name');
        $this->info('Your answer: '.$data);

        $user = $this->ask('Enter your database user');
        $this->info('Your answer: '.$user);

        $pass = $this->ask('Enter your database pass (write null if empty)');
        if ($pass == "null") {
            $pass = "";
            $this->warn('Your password is insecure!');
        }
        $this->info('Your answer: '.$pass);

        if($this->confirm('Conn: '.$conn.PHP_EOL.' Host: '.$host.PHP_EOL.' Port: '.$port.PHP_EOL.' Data: '.$data.PHP_EOL.' User: '.$user.PHP_EOL.' Pass: '.$pass.PHP_EOL.PHP_EOL.' Are your credentials correct?', true)) {
            $this->vars['DB_CONNECTION'] = $conn;
            $this->vars['DB_HOST'] = $host;
            $this->vars['DB_PORT'] = $port;
            $this->vars['DB_DATABASE'] = $data;
            $this->vars['DB_USERNAME'] = $user;
            $this->vars['DB_PASSWORD'] = $pass;
        } else {
            $this->info(PHP_EOL.' Retrying');
            $this->getDatabase();
        }
    }

    private function createDatabase() {
        if($this->confirm('Do you want to create the database and the tables? [Rights required]', true)) {
            $conn = new \mysqli(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'));

            if ($conn->connect_error) {
                $this->error("Connection failed: " . $conn->connect_error);
            }

            $sql = "CREATE DATABASE IF NOT EXISTS `" . env('DB_DATABASE') . "`";
            if ($conn->query($sql) === TRUE) {
                $this->info('Database successfully created!');
                $this->info('Creating tables... (Ignore error 42S01)');
                try {
                    Artisan::call('migrate:install');
                } catch(\Exception $ex) {
                    if ($ex->getCode() != ("42S01" || 1050)) {
                        $this->error($ex->getMessage());
                    }
                }
                Artisan::call('migrate:refresh');

                $this->info('Tables successfully created!');
            } else {
                $this->error('Error creating database: ['.$conn->errno.'] ' . $conn->error);
            }
        }
    }
}
