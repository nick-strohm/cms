<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    $viewCtrl = new \App\Http\Controllers\ViewController();
    return $viewCtrl->getBaseLayout();
});

Route::get('/sidebar', function () {
    $viewCtrl = new \App\Http\Controllers\ViewController();
    return $viewCtrl->getSidebarLayout();
});

/*Route::get('/', function () {
    $settings = array(
        'navbar' => true,
        'navbarItems' => array(
            array(
                'type' => 'single',
                'name' => 'Home',
                'url' => '/',
                'perm' => 'page.view.home',
            ),
            array(
                'type' => 'stacked',
                'name' => 'Parent',
                'url' => '#stacked_parent',
                'perm' => 'page.view.home',
                'items' => array(
                    array(
                        'type' => 'single',
                        'name' => 'SubItem',
                        'url' => '/sidebar',
                        'perm' => 'page.view.home'
                    )
                )
            )
        )
    );
    return view('layouts.baseLayout', ['settings' => $settings]);
});

Route::get('/sidebar', function() {
    $settings = array(
        'navbar' => true,
        'navbarItems' => array(
            array(
                'type' => 'single',
                'name' => 'Home',
                'url' => '/',
                'perm' => 'page.view.home',
            ),
            array(
                'type' => 'stacked',
                'name' => 'Parent',
                'url' => '#stacked_parent',
                'perm' => 'page.view.home',
                'items' => array(
                    array(
                        'type' => 'single',
                        'name' => 'SubItem',
                        'url' => '/sidebar',
                        'perm' => 'page.view.home'
                    )
                )
            )
        ),
    );

    $sidebar = array(
        'orientation' => 'right',
        'links' => array(
            array(
                'url' => '/',
                'name' => 'Home',
            ),
            array(
                'url' => '/sidebar',
                'name' => 'Sidebar',
            ),
        ),
    );

    $errors = array(
        array(
            'type' => 'danger',
            'text' => 'Danger'
        ),
        array(
            'type' => 'info',
            'text' => 'Info'
        ),
        array(
            'type' => 'warning',
            'text' => 'Warning'
        ),
        array(
            'type' => 'success',
            'text' => 'Success'
        ),
    );

    return view('layouts.sidebarLayout', ['settings' => $settings, 'sidebar' => $sidebar, 'errors' => $errors]);
});*/

Route::auth();

Route::get('/home', 'HomeController@index');
