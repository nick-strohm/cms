<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ViewController extends Controller
{
    protected $settings = array();
    protected $navbar   = array();
    protected $sidebar  = array();
    protected $errors   = array();

    function __construct() {
        $this->settings = array(
            'navbar' => true
        );

        $this->loadNavigation();

        $this->errors = array(
            array(
                'type' => 'danger',
                'text' => 'Danger'
            ),
            array(
                'type' => 'info',
                'text' => 'Info'
            ),
            array(
                'type' => 'warning',
                'text' => 'Warning'
            ),
            array(
                'type' => 'success',
                'text' => 'Success'
            ),
        );

    }

    protected function loadNavigation() {
        $results = DB::select('SELECT * FROM nav_items WHERE parent = 0');
        foreach($results as $result) {
            $item = array(
                'type' => $result->type,
                'name' => $result->name,
                'url' => $result->url,
                'perm' => $result->permission
            );

            if ($item['type'] == "stacked") {
                $item['items'] = $this->getNavbarSubItems($result->id);
            }

            array_push($this->navbar, $item);
        }
    }

    protected function getNavbarSubItems($id) {
        $results = DB::select('SELECT * FROM nav_items WHERE parent = :id', ['id' => $id]);
        $items = array();
        foreach($results as $result) {
            if ($result->type != "single")
                continue;

            $item = array(
                'type' => $result->type,
                'name' => $result->name,
                'url' => $result->url,
                'perm' => $result->permission
            );

            array_push($items, $item);
        }

        return $items;
    }

    protected function showLayout($layout) {
        return view($layout, [
            'settings' => $this->settings,
            'navbar' => $this->navbar,
            'sidebar' => $this->sidebar,
            'errors' => $this->errors,
        ]);
    }

    function getBaseLayout($layout = 'layouts.baseLayout') {
        return $this->showLayout($layout);
    }

    function getSidebarLayout($layout = 'layouts.sidebarLayout') {
        $this->sidebar = array(
            'orientation' => 'right',
            'links' => array(
                array(
                    'url' => '/',
                    'name' => 'Home',
                ),
                array(
                    'url' => '/sidebar',
                    'name' => 'Sidebar',
                ),
            ),
        );

        return $this->showLayout($layout);
    }
}
